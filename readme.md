##Sample application

Sample application that calculates max profit when buying gold.

Calculation is divided in two steps: getting data and using that data for calculations.

### Installation

Clone repository to a directory.

Run composer install to install all dependencies.

### Usage

Run php -f artisan gold:fetch to get data from provider.
Running this again will remove old data and gets new set from provider. 

Run php -f artisan gold:calculate 5 to get top 5 dates to buy gold in the past and some other useful information.
This method uses data from first step - it will work offline.

Run phpunit tests/ to run unit tests
