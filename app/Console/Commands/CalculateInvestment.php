<?php
namespace App\Console\Commands;

use App\Services\Gold\Calculator;
use Illuminate\Console\Command;

/**
 * Class CalculateInvestment
 *
 * @package App\Console\Commands
 */
class CalculateInvestment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gold:calculate {number=3} {amount=600000}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate best investment date';

    /**
     * @var Calculator
     */
    protected $calculator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Calculator $calculator)
    {
        parent::__construct();
        $this->calculator = $calculator;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // arguments
        $number = $this->argument('number');
        $amount = $this->argument('amount');

        $best   = $this->calculator->calculate(
            $amount,
            $number
        );

        // prepare table header
        $header = ['Date', 'Price for 1g', 'Weight of gold (in g)', 'Max profit with that price', 'Date', 'Max price for 1g','Profit with current price'];
        // @todo migrate to
        // $fmt = new \NumberFormatter( 'pl_PL', \NumberFormatter::CURRENCY );

        // format numbers for display
        foreach($best as &$row){
            $row['max profit']      = number_format($row['max profit'], 2, ',', ' ').' PLN';
            $row['current profit']  = number_format($row['current profit'], 2, ',', ' ').' PLN';
        }
        
        $this->info("Best times to buy gold:");
        $this->table($header, $best);
    }
}
