<?php
namespace App\Contracts;

/**
 * Interface DataSource
 *
 * @package App\Contracts
 */
interface DataSource {

    /**
     * @param $start
     * @param $end
     *
     * @return mixed
     */
    public function get($start, $end);
}
